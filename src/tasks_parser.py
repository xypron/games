#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
Copyright 2015, Markus Koschany <apo@debian.org>

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this package; if not, write to the Free Software Foundation, Inc., 51 Franklin
St, Fifth Floor, Boston, MA 02110-1301 USA
'''

import os

OLD_GAMES = "games_old"
blacklisted_tasks = ("c++-dev", "content-dev", "java-dev", "perl-dev",
                     "python3-dev")

def get_current_games():
    games = []
    tasks_directory = os.path.abspath(
        os.path.join(
            os.path.dirname(__file__),
            os.pardir,
            'tasks'))
    for filename in os.listdir(tasks_directory):
        if filename in blacklisted_tasks:
            continue
        with open(os.path.join(tasks_directory, filename)) as f:
            for line in f:
                try:
                    depends_list = line.split()
                    if depends_list[0].startswith((
                        "Depends:",
                        "Suggests:")):
                        games.append(depends_list[1])
                except (KeyError, IndexError):
                    pass

    with open(OLD_GAMES, mode="w", encoding="utf-8") as fout:
        for game in sorted(set(games)):
            fout.write("%s\n" % str(game))



get_current_games()
